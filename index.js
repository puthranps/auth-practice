const express = require('express');
const mongoose = require('mongoose');
require('./models/user');
require('./services/passport');

const { URI } = require('./config/database_connect.json');
const PORT = process.env.PORT || 5000;

mongoose.connect(URI); 

const app = express();

require('./routes/authRoutes')(app);

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});

