const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const mongoose = require('mongoose');
const { GOOGLE_ID, GOOGLE_SECRET } = require('../config/keys.json');

const User = mongoose.model('user');

passport.use(new GoogleStrategy({
  clientID : GOOGLE_ID, 
  clientSecret : GOOGLE_SECRET,
  callbackURL : '/auth/google/callback'
  },(accesstoken, refreshToken, profile, done) => {
      new User({ googleId : profile.id }).save();
  })
);